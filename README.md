# YouTube Subscriptions JSON to OPML

Simple Python script to transform a YouTube subscriptions JSON acquired from Google Takeout to OPML for consumption in an RSS reader

Place the `subscriptions.json` file in the same directory as the script, and then run it like this: `python3 youtube_json2opml.py`. If it succeeds it will create a new file called `youtube_subscriptions.opml`.
