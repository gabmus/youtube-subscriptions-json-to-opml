import json

out = '''
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head>
    <title>Subscriptions</title>
  </head>
  <body>
'''

with open('subscriptions.json', 'r') as fd:
    yt = json.loads(fd.read())

for chan in yt:
    out += '''
        <outline
            title="{0}"
            text=""
            type="rss"
            xmlUrl="https://www.youtube.com/feeds/videos.xml?channel_id={1}"
            htmlUrl="https://www.youtube.com/channel/{1}"
            category="YouTube"
        />
    '''.format(
        chan['snippet']['title'],
        chan['snippet']['resourceId']['channelId']
    )

out += '''
  </body>
</opml>
'''

with open('youtube_subscriptions.opml', 'w') as fd:
    fd.write(out.strip())
